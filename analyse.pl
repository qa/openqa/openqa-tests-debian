use v5.10;
use strict;
use warnings;
use autodie;
use JSON qw( decode_json );

# Analyse the variables in the Perl code
#
# Invoke with:
# rgrep -E "get_var|get_required_var|set_var|check_var|get_var_array|check_var_array" -l --include="*.pm" | perl analyse.pl
#
my %variables;

sub extract_arguments {
    my ($line, $functionname) = @_;

    my ($key, $arg2);
    my $found = $line =~ /
		    ${functionname}[(] # The function and its opening bracket
		    ([^)]+)	       # Anything except the closing bracket
		    [)]		       # The closing bracket
		/x;
    if ($found) {
        ($key, $arg2) = $1 =~ /
        	["']	  # Opening quote
        	([^"']*)  # Key value
        	["']      # Closing quote
        	,?        # Optional second argument
        	\s*       # Trim leading whitespace
        	(.*)      # Value of the second argument
        	/x;
        $arg2 =~ s/"//g;
        $arg2 =~ s/'//g;
    } else {
        say "W: ${functionname} without brackets: $line";
    }
    return ($found, $key, $arg2);
}

sub analyse_line {
    my ($line) = @_;

    if ($line =~ /get_var/) {
        my ($found, $key, $default) = extract_arguments $line, 'get_var';
        if ($found) {
            if (length($default) > 0) {
                $variables{$key}{'values'}{'default=' . $default} = 1;
            } else {
                $variables{$key}{'values'}{'<no default provided>'} = 1;
            }
            $variables{$key}{'<read>'} = 1;
        }
    } elsif ($line =~ /get_required_var/) {
        my ($found, $key) = extract_arguments $line, 'get_required_var';
        if ($found) {
            $variables{$key}{'values'}{'<required>'} = 1;
            $variables{$key}{'<read>'} = 1;
        }
    } elsif ($line =~ /set_var/) {
        my ($found, $key, $value) = extract_arguments $line, 'set_var';
        if ($found) {
            if (length($value) > 0) {
                $variables{$key}{'values'}{$value} = 1;
                $variables{$key}{'<write>'} = 1;
            } else {
                say "I: set_var without value: $line";
            }
        }
    } elsif ($line =~ /check_var/) {
        my ($found, $key, $value) = extract_arguments $line, 'check_var';
        if ($found) {
            if (length($value) > 0) {
                $variables{$key}{'values'}{$value} = 1;
                $variables{$key}{'<read>'} = 1;
            } else {
                say "I: check_var without value: $line";
            }
        }
    } elsif ($line =~ /get_var_array/) {
        say "I: get_var_array is not implemented yet: $line";
    } elsif ($line =~ /check_var_array/) {
        say "I: check_var_array is not implemented yet: $line";
    }
}

sub process_file {
    my ($filename) = @_;
    chomp $filename;
    open my $FILE, '<', $filename;
    while (<$FILE>) {
        if (/get_var|get_required_var|set_var|check_var|get_var_array|check_var_array/) {
            # A function name can occur several time on a single line
            s/get_var/\nget_var/g;
            s/get_required_var/\nget_required_var/g;
            s/set_var/\nset_var/g;
            s/check_var/\ncheck_var/g;
            s/get_var_arrray/\nget_var_array/g;
            s/check_var_array/\ncheck_var_array/g;
            my @keywords = split(/\n/);
            for my $keyword (@keywords) {
                analyse_line $keyword;
            }
        }
    }
    close $FILE;
}

sub process_json_settings {
    my ($settings) = @_;
    for my $key (sort keys %{$settings}) {
        $variables{$key}{'values'}{$settings->{$key}} = 1;
        $variables{$key}{'<write>'} = 1;
    }
}

sub process_json_templates {
    my $json_text = do {
        open(my $json_fh, '<:encoding(UTF-8)', 'templates.fif.json');
        local $/;
        <$json_fh>;
    };
    my $data = decode_json $json_text;

    foreach my $key (sort keys %{$data->{'TestSuites'}}) {
        process_json_settings $data->{'TestSuites'}->{$key}->{'settings'};
    }
    foreach my $key (sort keys %{$data->{'Machines'}}) {
        process_json_settings $data->{'Machines'}->{$key}->{'settings'};
    }
    foreach my $key (sort keys %{$data->{'Products'}}) {
        process_json_settings $data->{'Products'}->{$key}->{'settings'};
    }
    foreach my $key (sort keys %{$data->{'Profiles'}}) {
        process_json_settings $data->{'Profiles'}->{$key}->{'settings'};
    }
}

# Read a list of files to process
while (<STDIN>) {
    process_file $_;
}
process_json_templates;

say 'Variables that are not read in the Perl code, but are set:';
foreach my $key (sort keys %variables) {
    if (exists $variables{$key}{'<write>'} && !exists $variables{$key}{'<read>'}) {
        say '  ' . $key . ': ' . join(',', sort keys %{$variables{$key}{'values'}});
    }
}
say '-----';
say 'Variables that are read in the Perl code, but are not set:';
say 'These might be variables that are set in the openqa-cli command, or originate from Redhat';
foreach my $key (sort keys %variables) {
    if (!exists $variables{$key}{'<write>'} && exists $variables{$key}{'<read>'}) {
        say '  ' . $key . ': ' . join(',', sort keys %{$variables{$key}{'values'}});
    }
}
say '-----';
say 'Regularly used variables:';
foreach my $key (sort keys %variables) {
    if (exists $variables{$key}{'<write>'} && exists $variables{$key}{'<read>'}) {
        say '  ' . $key . ': ' . join(',', sort keys %{$variables{$key}{'values'}});
    }
}

