# Debian's openQA tests
#
# Copyright 2023 Philip Hands, Roland Clobus
#
# This script needs to be provided with parameters:
# downs: the number of times we should go down, starting from the top locale
### menu_path: the path through the menu
### needle: (optional) the destination needle to look for
###         when not provided, the menu_path should not be reachable
#
### Debug options:
###  skip_installer_items

use base 'basetest';
use strict;
use warnings;
use testapi;
use utils;

# my $skip_installer_items = 0;    # Set to 1 to skip the installer menu items

my $missing_glyphs_found = 0;

sub check_mising_glyph {
    if (check_screen 'missing_glyph', 1) {
        $missing_glyphs_found = 1;
        record_soft_failure "Missing Glyphs found -- fonts missing?";
    }
    save_screenshot;
}

sub run {
    my ($self, $run_args) = @_;
    die 'Needs argument "downs" to know which test to run' unless $run_args && defined($run_args->{downs});
    my $downs = $run_args->{downs};
    my $last = $run_args->{last};
    # my @menu_path = @{$run_args->{menu_path}};
    # my $needle    = $run_args->{needle};

    # assert_screen 'language_subprompt';

    foreach (1 .. ($downs-1)) {
        send_key "down";
    }

    wait_still_screen 2;

    if ($downs > 0) {
        if (! wait_screen_change( sub { send_key 'down'; }, 5)) {
            record_soft_failure("The screen did not change when pressing the last 'down'. "
                                . 'The test script needs to be updated.');
        }
    }

    check_mising_glyph;

    if ($last) {
        if (wait_screen_change( sub { send_key 'down'; }, 5)) {
            record_soft_failure("The screen changed when pressing 'down' one extra time, after $downs times. "
                                . 'The test script needs to be updated.');
            send_key "up";
            wait_still_screen 1;
            save_screenshot;
        }
    }

    foreach (1..3) {
        send_key 'ret';
        wait_still_screen 1;
        check_mising_glyph;
    }
    return;
}

# there's not much point trying to collect post-fail data, so let's not
sub post_fail_hook {
}

sub test_flags {
    return {always_rollback => 1};
}

1;
