use base "installedtest";
use strict;
use testapi;
use utils;

my $gnome_editor = is_at_least_debver('bookworm') ? "gnome-text-editor"    : "gedit";
my $gnome_dest   = is_at_least_debver('bookworm') ? "Documents/output.pdf" : "'Documents/Untitled Document 1.pdf'";

#<<< No auto-formatting by 'make tidy'
my (%editor,%viewer,%maximize,%pdf_dest);
my        @key  = ( 'cinnamon',  'gnome',       'kde',         'lxde',         'lxqt',         'mate',     'xfce');
  @editor{@key} = ( 'gedit',      $gnome_editor,'kwrite',      'mousepad',     'featherpad',   'pluma',    'mousepad');
  @viewer{@key} = ( 'evince',    'evince',      'okular',      'evince',       'qpdfview',     'atril',    'atril');
@maximize{@key} = (['alt-f10'], ['super-up'],  ['super-pgup'],['alt-spc','x'],['alt-spc','x'],['alt-f10'],['alt-f10'] );
@pdf_dest{@key} = ("'Documents/Untitled Document 1.pdf'",
                                  $gnome_dest,
                                                "Untitled.pdf",
                                                               "Documents/output.pdf",
                                                                               "Untitled.pdf",
                                                                                              "'Documents/Unsaved Document 1.pdf'",
                                                                                                           "Documents/output.pdf" );

#>>> Turn auto-formatting on again

sub run {
    my $self      = shift;
    my $user_home = "/home/" . get_required_var("USER_LOGIN");

    my $desktop = get_required_var("DESKTOP");
    # Some simple variances between desktops.

    # Open the text editor and print the file.
    menu_launch_type "$editor{$desktop}";
    assert_screen "apps_run_texteditor";
    assert_and_click "editor_new_document" if ($desktop eq "kde" && is_at_least_debver('bookworm'));
    wait_still_screen 2;    # aarch64 had been missing 'shift' going down for the 'T'
    type_string 'The quick brown fox jumps over the lazy dog.';
    wait_still_screen 1;

    # Print the file using the default printer
    send_key "ctrl-p";
    assert_screen "printing_selection";
    if (match_has_tag "printing_select_defaultpdfprinter") {
        record_soft_failure "PDF printer was not selected by default, feature request at https://gitlab.gnome.org/GNOME/gtk/-/issues/6766";
        assert_and_click "printing_select_defaultpdfprinter";
    }
    elsif (!match_has_tag "printing_defaultpdfprinter_ready") {
        die "'printing_selection' needle needs one of 'printing_defaultpdfprinter_ready' or 'printing_select_defaultpdfprinter' to also be set";
    }
    if ($desktop eq 'kde' && is_at_least_debver('trixie')) {
        assert_and_click "printing_enter_filename";
        type_string $pdf_dest{$desktop};

    }
    assert_and_click "printing_print";

    # Exit the application
    send_key "alt-f4";
    assert_and_click "dont_save_document";
    # Wait out confirmation on GNOME
    if (check_screen "printing_print_completed", 1) {
        sleep 30;
    }

    # Open the pdf file and check the print
    menu_launch_type "$viewer{$desktop} $pdf_dest{$desktop}";
    wait_still_screen 5;
    # Resize the window, so that the size of the document fits the bigger space
    # and gets more readable.
    for (@{$maximize{$desktop}}) { send_key $_; }
    if ($desktop eq "kde") {
        wait_still_screen 2;     # allow the above maximise to settle
        send_key "ctrl-home";    # Make sure we're at the start of the document
    }
    # Check the printed pdf.
    assert_screen "printing_check_sentence";
}


sub test_flags {
    return {fatal => 1};
}

1;

# vim: set sw=4 et:
