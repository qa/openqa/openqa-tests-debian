# SUSE's openQA tests
#
# Copyright © 2009-2013 Bernhard M. Wiedemann
# Copyright © 2012-2017 SUSE LLC
# Copyright ©      2017 Philip Hands
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Summary: Bootloader to setup boot process with arguments/options
# Maintainer: Jozef Pupava <jpupava@suse.com>

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    if (check_var('OFFLINE_SUT', '1')) {
        # if OFFLINE_SUT==1, we intend to run without a network card
        assert_screen 'NoEthernetCard', check_var('FLAVOR', 'mini-iso') ? 30 : 1 ;
        send_key 'ret';
        assert_screen 'NoNetworkInterfaces';
        send_key 'ret';

        assert_screen 'EnterTheHostname';
        send_key 'ret';
    }
    else {
        assert_screen 'EnterTheHostname', check_var('FLAVOR', 'mini-iso') ? 30 : 1 ;
        send_key 'ret';

        assert_screen 'DomainName';
        send_key 'ret';
    }
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
