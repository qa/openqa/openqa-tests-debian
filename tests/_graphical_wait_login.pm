use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my $self       = shift;
    my $user_login = get_var("USER_LOGIN",    "testy");
    my $password   = get_var("USER_PASSWORD", "weakpassword");
    # Set DUMP_BOOT_CONFIGURATION to 1 to have the actual boot variables in a record_info section
    my $dump_boot_configuration = get_var("DUMP_BOOT_CONFIGURATION", 0);

    # if UPGRADE, we have to wait for the entire upgrade
    # unless ENCRYPT_PASSWORD is set (in which case the postinstall
    # test does the waiting)
    my $wait_time = 300;
    $wait_time = 6000 if (get_var("UPGRADE") && !get_var("ENCRYPT_PASSWORD"));
    my $desktop = get_var('DESKTOP');

    if ($dump_boot_configuration) {
        my $bootlive = get_var('BOOT_LIVE',         'unset');
        my $bootfrom = get_var('BOOTFROM',          'unset');
        my $islive   = get_var('IS_LIVE',           'unset');
        my $auto     = get_var('CONSOLE_AUTOLOGIN', 'unset');

        record_info('bootinfo', "BOOT_LIVE: $bootlive BOOTFROM: $bootfrom IS_LIVE: $islive USER_LOGIN: $user_login USER_PASSWORD: $password CONSOLE_AUTOLOGIN: $auto", result => 'ok');
    }

    # handle bootloader, if requested
    if (get_var("GRUB_POSTINSTALL")) {
        do_bootloader(postinstall => 1, params => get_var("GRUB_POSTINSTALL"), timeout => $wait_time);
        $wait_time = 300;
    } elsif (get_var('BOOT_LIVE')) {
        do_bootloader(live => 1, timeout => $wait_time);
    }

    # provide sane defaults where DM_* variables were unset
    my $dm_needs_username =
      get_var('DM_NEEDS_USERNAME',
              (!defined($desktop) ||    # text mode
               (grep { $desktop eq $_ } qw(cinnamon mate ldxe xfce kali)) ||
               ($desktop eq 'lxqt' && !is_at_least_debver('trixie'))
              )
             );

    my $dm_remembers_username =
      get_var('DM_REMEMBERS_USERNAME',
              ($desktop eq 'lxqt'));

    my $dm_select_password_keystroke =
      get_var('SELECT_PASSWORD_KEYSTROKE',
              ($desktop eq 'lxqt') ? 'tab' : 'ret' );

    # Wait for the login screen, unless we're booting into the live
    # environment, which boots directly to the desktop or
    # if USER_LOGIN is set to string 'false'
    unless (get_var('BOOT_LIVE') || $user_login eq "false") {
        boot_to_login_screen(timeout => $wait_time);

        # GDM 3.24.1 dumps a cursor in the middle of the screen here...
        mouse_hide;
        if (get_var("DESKTOP") eq 'gnome' ||
            get_var("DESKTOP") eq 'gnome_flashback') {
            # we have to hit enter to get the password dialog, and it
            # doesn't always work for some reason so just try it three
            # times
            send_key_until_needlematch("graphical_login_input", "ret", 3, 5);
        } elsif (get_var('DESKTOP') eq 'lxqt' &&
            is_at_least_debver('trixie') &&
            match_has_tag('graphical_login_onscreenkeyboard')) {
            click_lastmatch;
        }
        # be ready to retype the password, as it sometimes fails
        # sadly, this doesn't help with mis-typed usernames.
        for my $try (0 .. 3) {
            # graphical_login_username: Username is not provided, focus field is the username
            # graphical_login_input: Username has been entered, focus field is the password
            assert_screen ['graphical_login_username', 'graphical_login_input'], 90;
            if (match_has_tag('graphical_login_username')) {
                type_string $user_login;
                send_key $dm_select_password_keystroke;
            }
            assert_screen "graphical_login_input";

            my $pw_maybe = $password;
            $pw_maybe .= "X" if (0 == $try);    # break the password on the first pass
            if (get_var("SWITCHED_LAYOUT")) {
                # see _do_install_and_reboot; when layout is switched
                # user password is doubled to contain both US and native
                # chars
                desktop_switch_layout 'ascii';
                type_string $pw_maybe;
                desktop_switch_layout 'native';
                type_string $pw_maybe;
            }
            else {
                type_string $pw_maybe;
            }
            wait_still_screen(stilltime => 2, similarity_level => 35);
            save_screenshot;
            wait_screen_change { send_key "ret"; };

            if ($try == 0) {
                assert_screen([qw(authentication_failed graphical_login_input)], timeout => 10, no_wait => 1);
                record_soft_failure "Failed to notice the 'authentication_failed' warning that should result from intentionally entering the wrong password"
                  unless (match_has_tag('authentication_failed'));
            } else {
                if (check_screen "authentication_failed", 5) {
                    # expect initial failure, but record a soft failure otherwise (to notice typo issues)
                    record_soft_failure "Authentication failed (try: $try)";
                } else {
                    last;
                }
            }
        }
    }

    # For GNOME, handle initial-setup or welcome tour, unless START_AFTER_TEST
    # is set in which case it will have been done already. Always
    # do it if ADVISORY_OR_TASK is set, as for the update testing flow,
    # START_AFTER_TEST is set but a no-op and this hasn't happened
    if (get_var("DESKTOP") eq 'gnome'
        && (get_var("ADVISORY_OR_TASK")
            || !get_var("START_AFTER_TEST"))) {
        # as this test gets loaded twice on the ADVISORY_OR_TASK flow,
        # check whether this happened already
        gnome_initial_setup() unless (get_var("_SETUP_DONE"));
    }

    if (!get_var('_SETUP_DONE') && !get_var('START_AFTER_TEST')) {
        handle_kde_welcome_screen() if (check_var('DESKTOP', 'kde'));
    }

    # Move the mouse somewhere it won't highlight the match areas
    # or shows pop-ups for the running program or taskbar (e.g. in KDE)
    if (get_var("DESKTOP") eq 'xfce') {
        # The taskbar is in the middle of the screen, use the left bottom corner
        mouse_set(0, get_var('YRES', 768));
    } else {
        # At the bottom, in the middle
        mouse_set(get_var('XRES', 1024) / 2, get_var('YRES', 768));
    }
    # KDE can take ages to start up
    check_desktop(timeout => (check_var('DESKTOP', 'kde') ? 200 : 150));
}

sub test_flags {
    my %ret = (fatal => 1);

    # FIXME: we should decide on some criteria that trigger
    # the need for a milestone here, in the meantime we have a variable that's
    # not really intended to be defined
    $ret{milestone} = 1 if get_var('GRAPHICAL_WAIT_LOGIN_MILESTONE');
    return \%ret;
}

1;

# vim: set sw=4 et:
