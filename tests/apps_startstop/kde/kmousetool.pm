use base "installedtest";
use strict;
use testapi;
use utils;

# This test checks that KMouseTool starts.

sub run {
    my $self = shift;

    # Start the application
    menu_launch_type 'kmousetool';
    # Check that it is started
    assert_screen 'kmousetool_runs', timeout => 60;
    if (!is_at_least_debver('trixie')) {
        # kmousetool 24.12.0 (trixie) ignores a simple click on the tray icon
        # so this bit of the test no longer works for trixie or later.

        # Close the application
        quit_with_shortcut();
        # Open from the system tray
        assert_and_click 'kmousetool_systemtray';
        assert_screen 'kmousetool_runs';
    }
    # Really close
    send_key 'alt-q';
    assert_screen 'workspace';
}

sub test_flags {
    return {};
}


1;

# vim: set sw=4 et:
