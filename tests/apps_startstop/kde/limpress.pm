use base "installedtest";
use strict;
use testapi;
use utils;

# This test checks that LibreOffice Impress starts.

sub run {
    my $self = shift;

    # Start the application
    menu_launch_type 'libreoffice impress';
    # Dismiss 'tip of the day' if necessary
    lo_dismiss_tip;
    # Check that is started
    assert_and_click 'apps_run_limpress_start', timeout => 60;
    # Gnome 40 does the tip later, so check again
    lo_dismiss_tip;

    assert_screen 'limpress_runs';
    # Register application
    quit_with_shortcut();
}

sub test_flags {
    return {};
}

1;

# vim: set sw=4 et:
