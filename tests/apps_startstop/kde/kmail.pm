use base "installedtest";
use strict;
use testapi;
use utils;

# This test checks that Kmail starts.

sub run {
    my $self = shift;

    # Start the application
    menu_launch_type 'kmail';
    # The Kmail window is now often covered with an account creation dialogue.
    # There is a needle for each case (Account Assistant on top, or underneath)
    # and we click on that window to make sure it's on top before closing it.
    if (check_screen("kmail_account_dialogue", timeout => 45)) {
        # Click on the dialogue window, to make sure it's on top
        wait_screen_change { click_lastmatch; };
        # Click on the exit icon
        assert_and_click("kde_exit_icon");
    }
    assert_screen("kmail_runs");

    # Close the application
    quit_with_shortcut();
}

sub test_flags {
    return {always_rollback => 1};
}


1;

# vim: set sw=4 et:
