use base "installedtest";
use strict;
use testapi;
use utils;

# This test checks that Akregator starts.

sub run {
    my $self = shift;
    # Start the application
    menu_launch_type('QT_LOGGING_RULES=@*=true@ akregator');
    # Check that it is started
    assert_screen 'akregator_runs', timeout => 60;
    # Close the application
    quit_with_shortcut();
    # Open from the system tray
    assert_and_click 'akregator_systemtray', timeout => 120;
    assert_screen 'akregator_runs';
    # Really close
    send_key 'ctrl-q';
    assert_screen 'workspace';
}

sub test_flags {
    return {};
}


1;

# vim: set sw=4 et:
