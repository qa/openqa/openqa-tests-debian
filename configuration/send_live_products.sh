#!/bin/sh
#
# Upload product definitions for the live images
#
# Requires:
#   live_products.json: product definition, where version equals "*"
#
for distribution in bullseye bookworm trixie sid;
do
       sed -e s/"*"/"${distribution}"/ live_products.json | openqa-load-templates --update /dev/stdin;
done
