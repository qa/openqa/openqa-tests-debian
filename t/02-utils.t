use Test2::V0;
use Test2::Tools::Exception;
# strict and warnings are on for you now.

use utils   qw(is_at_least_debver);
use testapi qw(check_var get_var set_var);

set_var('VERSION', "stable");
is(get_var('VERSION'), "stable");
is(is_at_least_debver("stable"),       T());
is(is_at_least_debver("testing"),      F());
is(is_at_least_debver("sid"),          F());

set_var('VERSION', "unstable");
is(get_var('VERSION'), "unstable");
is(is_at_least_debver("stable"),       T());
like(dies { is_at_least_debver("buzz") },
     qr/Version 'buzz' is not recognised/);
is(is_at_least_debver("testing"),      T());
is(is_at_least_debver("unstable"),     T());

set_var('VERSION', "testing_something");
is(is_at_least_debver("stable"),       T());
is(is_at_least_debver("testing"),      T());
is(is_at_least_debver("unstable"),     F());

set_var('VERSION', "oldstable");
is(is_at_least_debver("stable"),       F());
is(is_at_least_debver("testing"),      F());
is(is_at_least_debver("sid"),          F());

set_var('VERSION', "rc-buggy");
like(dies { is_at_least_debver("stable") },
     qr/Unrecognised codename: "rc" .* VERSION="rc-buggy"/);

set_var('VERSION', undef);
like(dies { is_at_least_debver("stable") },
     qr/Could not retrieve required variable VERSION/);

done_testing;
