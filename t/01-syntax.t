#!/bin/perl

use FindBin;
unshift @INC, "/usr/lib/os-autoinst", "$FindBin::Bin/../lib", "$FindBin::Bin/lib";

use Test::Strict;
all_perl_files_ok(qw 'products/debian/main.pm lib tests');
