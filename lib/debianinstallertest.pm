package debianinstallertest;
use base 'basetest';

# base class for all Anaconda (installation) tests

# should be used in tests where Anaconda is running - when it makes sense
# to upload Anaconda logs when something fails. Many tests using this as a
# base likely will also want to `use anaconda` for commonly-used functions.

use strict;
use testapi;
use utils;

sub post_fail_hook {
    my $self = shift;

    if (check_var('DISTRI', 'fedora')) {    # this bit is not useful on Debian
                                            # if error dialog is shown, click "report" - it then creates directory structure for ABRT
        my $has_traceback = 0;
        if (check_screen "anaconda_error", 10) {
            assert_and_click "anaconda_error_report";
            $has_traceback = 1;
        } elsif (check_screen "anaconda_text_error", 10) {    # also for text install
            type_string "1\n";
            $has_traceback = 1;
        }
    }

    save_screenshot;
    $self->root_console();

    # this is here to try to kill X if it's constantly trying to restart, and dying
    sleep 1;
    type_string "chmod -x /usr/bin/Xorg\n";
    sleep 1;
    type_string "\n";
    sleep 1;
    type_string "chmod -x /usr/bin/Xorg\n";
    sleep 1;
    type_string "\n";
    sleep 5;
    # kludge the keyboard to avoid typing @ instead of " and the like
    console_loadkeys_us;

    # First, let's check for the obvious:
    if (0 == (script_run("grep -n 'No space left on device' /var/log/syslog > /dev/${serialdev}", timeout => 60) // 1)) {
        record_info("No Space", "'No space left on device' seen in syslog", result => 'fail');
    }

    script_run("df -h > /tmp/df.txt");
    script_run("awk '/^Package: /{ p = \$2 };/Version: /{ v = \$2 };/^\$/{ printf \"%-40s %s\\n\", p, v }' /var/lib/dpkg/status > /tmp/DI-installed-pkgs.txt");
    script_run("chroot /target dpkg -l > /tmp/target-installed-pkgs.txt");
    if (make_sure_curl_is_available()) {
        upload_logs '/tmp/df.txt';
        upload_logs '/var/log/Xorg.0.log',          log_name => 'Xorg.0.log.txt', failok => 1;
        upload_logs '/target/etc/apt/sources.list', log_name => 'in-target_sources.list.txt';
        upload_logs '/var/log/syslog',              log_name => 'DI_syslog.txt';
        upload_logs '/tmp/DI-installed-pkgs.txt';
        upload_logs '/tmp/target-installed-pkgs.txt';

        # Upload /var/log
        if (0 == (script_run "type gzip && tar -czf /tmp/var_log.tar.gz /var/log" // 1)) {
            upload_logs "/tmp/var_log.tar.gz";
        }
        elsif (0 == (script_run "tar -cf /tmp/var_log.tar /var/log" // 1)) {
            upload_logs "/tmp/var_log.tar";
        }
    }
    else {
        # splurge these at the serial port, to get it into the OpenQA logs without curl
        script_run("cat /tmp/df.txt > /dev/${serialdev}");
        script_run("cat /var/log/Xorg.0.log > /dev/${serialdev}");
        script_run("head -n-0 /etc/apt/sources.list /target/etc/apt/sources.list 2>/dev/null > /dev/${serialdev}");
        script_run("head -1000 /var/log/syslog > /dev/${serialdev}");
        script_run("tail -2000 /var/log/syslog > /dev/${serialdev}");
        script_run("cat /tmp/DI-installed-pkgs.txt > /dev/${serialdev}");
        script_run("cat /tmp/target-installed-pkgs.txt > /dev/${serialdev}");
    }
}

sub root_console {
    # Switch to an appropriate TTY and log in as root.
    my $self = shift;
    my %args = (
        @_);
    send_key "ctrl-alt-f2";
    console_login(user => "root");
}

1;

# vim: set sw=4 et:
