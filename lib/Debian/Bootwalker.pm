package Bootwalker;
use Moose;

has distri     => (isa => 'Str',      is => 'ro');
has flavor     => (isa => 'Str',      is => 'ro');
has bootloader => (isa => 'Str',      is => 'ro');
has entries    => (isa => 'ArrayRef', is => 'rw', default => sub { [] });

sub register_tests {
    my $self = shift;

    if ($#{$self->entries} >= 0) {
        # clearly the rest of this should be OO too,
        # but cheat for now by just grabbing the array ref
        register_bootmenu(\@{$self->entries}, [], 0);
    }
    return;
}

sub register_bootmenu {
    my ($mref, $path_to_here_ref, $submenuindex) = @_;
    my $bootwalk_args;
    use OpenQA::Test::RunArgs;

    my $counter = $submenuindex // 0;
    foreach my $element (@{$mref}) {
        my $cur_path_ref = [@{$path_to_here_ref}, $counter];

        my $testname = 'bootwalk_' . join(':', @{$cur_path_ref});

        if (defined($element->{needle})) {
            $bootwalk_args                   = OpenQA::Test::RunArgs->new();
            $bootwalk_args->{needle}         = $element->{needle};
            $bootwalk_args->{menu_path}      = $cur_path_ref;
            $bootwalk_args->{speechduration} = $element->{speechduration};

            autotest::loadtest('tests/boot_walk_once.pm', name => $testname, run_args => $bootwalk_args);
        }
        if (defined($element->{submenu})) {
            register_bootmenu(@{$element->{submenu}},
                $cur_path_ref, $element->{submenuindex});
        }
        $counter += 1;
    }
    my $final_path_ref  = [@{$path_to_here_ref}, $counter];
    my $final_path_name = [@{$path_to_here_ref}, 'last'];
    my $testname        = 'bootwalk_' . join(':', @{$final_path_name});
    $bootwalk_args              = OpenQA::Test::RunArgs->new();
    $bootwalk_args->{needle}    = undef;
    $bootwalk_args->{menu_path} = $final_path_ref;

    autotest::loadtest('tests/boot_walk_once.pm', name => $testname, run_args => $bootwalk_args);
}

sub prepare_bootmenu_debian_mini_iso {
    my $is_uefi = get_var('UEFI');
    my @menu;

    if ($is_uefi) {
        # The UEFI menu structure differs from the BIOS menu structure
        return [];
    }

    my @advanced_options_menu;
    my @accessible_dark_contrast_installer_menu;
    @menu = [
        {needle => 'debianinstaller_select_install_lang'},    # Graphical install
        {needle => 'netinst_bios_advanced_options_menu',
            submenu => \@advanced_options_menu},
        {needle => 'netinst_bios_dark_menu',
            submenu => \@accessible_dark_contrast_installer_menu, submenuindex => -1},
        {needle => 'netinst_bios_help'},
        {needle => 'speech_select_install_lang'}              # Install with speech synthesis
    ];

    my @speech_enabled_advanced_options_menu;
    @advanced_options_menu = [
        {needle => 'netinst_bios_main_menu'},                              # Back
        {needle => 'debianinstaller_main_menu'},                           # Expert
        {needle => 'debianinstaller_select_install_lang'},                 # Rescue
        {needle => 'debianinstaller_download_preconfiguration_file'},      # Automated
        {needle => 'netinst_bios_speech_enabled_advanced_options_menu',    # Speech
            submenu => \@speech_enabled_advanced_options_menu}
    ];

    @speech_enabled_advanced_options_menu = [
        {needle => 'netinst_bios_advanced_options_menu'},                  # Back
        {needle => 'speech_main_menu'},                                    # Expert
        {needle => 'speech_select_install_lang'},                          # Rescue
        {needle => 'speech_download_preconfiguration_file'}                # Automated
    ];

    @accessible_dark_contrast_installer_menu = [
        {needle => 'netinst_bios_main_menu'},                              # Back
        {needle => 'debianinstaller_select_install_lang'},                 # Rescue
        {needle => 'debianinstaller_download_preconfiguration_file'},      # Automated
        {needle => 'netinst_bios_dark_advanced_options_menu',
            submenu => \@speech_enabled_advanced_options_menu}             # Speech
    ];

    my @accessible_dark_contrast_advanced_options_menu;
    @accessible_dark_contrast_installer_menu = [
        {needle => 'netinst_bios_main_menu'},                 # Back
        {needle => 'debianinstaller_select_install_lang'},    # Install
        {needle => 'netinst_bios_dark_advanced_menu',
            submenu => \@accessible_dark_contrast_advanced_options_menu},
        {needle => 'netinst_bios_help'}
    ];

    @accessible_dark_contrast_advanced_options_menu = [
        {needle => 'netinst_bios_dark_menu'},                           # Back
        {needle => 'debianinstaller_main_menu'},                        # Expert
        {needle => 'debianinstaller_select_install_lang'},              # Rescue
        {needle => 'debianinstaller_download_preconfiguration_file'}    # Automated
    ];
    return @menu;
}

sub prepare_bootmenu_debian_netinst_iso {
    my $is_uefi = get_var('UEFI');
    my $variant = $is_uefi ? 'uefi' : 'bios';

    my @advanced_options_menu;
    my @accessible_dark_contrast_menu;
    my $is_aarch64 = check_var("MACHINE", "aarch64");
    my $menu;

    my $needle_install1 = 'debianinstaller_select_install_lang';
    my $needle_install2 = 'text_select_install_lang';

    if ($is_aarch64) {
        # The text installer is mentioned first for aarch64
        $needle_install1 = 'text_select_install_lang';
        $needle_install2 = 'debianinstaller_select_install_lang';
    }

    $menu = [
        {needle => $needle_install1},    # Graphical install
        {needle => $needle_install2},    # Install
        {needle => "netinst_${variant}_advanced_options_menu",
            submenu => \@advanced_options_menu},
        {needle => "netinst_${variant}_dark_menu",
            submenuindex => $is_uefi ? 0 : -2, submenu => \@accessible_dark_contrast_menu},
        $is_uefi ? () : {needle => 'netinst_bios_help'},    # Help
        {needle => 'speech_select_install_lang',
            speechduration => 300},                         # Speech
    ];

    my @desktop_environment_menu;
    my @speech_enabled_advanced_options_menu;
    if ($is_aarch64) {
        @advanced_options_menu = [
            $is_uefi ? () : {needle => 'netinst_bios_main_menu'},            # Back
            {needle => 'debianinstaller_main_menu'},                         # Graphical expert install
            {needle => 'debianinstaller_select_install_lang'},               # Graphical rescue mode
            {needle => 'debianinstaller_download_preconfiguration_file'},    # Graphical automated install
            {needle => 'text_main_menu'},                                    # Expert install
            {needle => 'text_select_install_lang'},                          # Rescue mode
            {needle => 'text_download_preconfiguration_file'},               # Automated install
            {needle => 'speech_main_menu'},                                  # Expert install with speech synthesis
            {needle => 'speech_select_install_lang'},                        # Rescue mode with speech synthesis
            {needle => 'speech_download_preconfiguration_file'},             # Automated install with speech synthesis
            {needle => 'desktop_environment_menu',
                submenu => \@desktop_environment_menu},                      # Desktop environment menu
        ];
    } else {
        @advanced_options_menu = [
            $is_uefi ? () : {needle => 'netinst_bios_main_menu'},            # Back
            {needle => 'debianinstaller_main_menu'},                         # Graphical expert install
            {needle => 'debianinstaller_select_install_lang'},               # Graphical rescue mode
            {needle => 'debianinstaller_download_preconfiguration_file'},    # Graphical automated install
            {needle => 'text_main_menu'},                                    # Expert install
            {needle => 'text_select_install_lang'},                          # Rescue mode
            {needle => 'text_download_preconfiguration_file'},               # Automated install

            {needle => "netinst_${variant}_speech_enabled_advanced_options_menu",
                submenu => \@speech_enabled_advanced_options_menu},
        ];
    }

    @speech_enabled_advanced_options_menu = [
        $is_uefi ? () : {needle => 'netinst_bios_advanced_options_menu'},    # Back
        {needle => 'speech_main_menu',
            speechduration => 40},                                           # Expert
        {needle => 'speech_select_install_lang',
            speechduration => 300},                                          # Rescue
        {needle => 'speech_download_preconfiguration_file',
            speechduration => 150},                                          # Automated
    ];

    my @accessible_dark_contrast_advanced_menu;
    @accessible_dark_contrast_menu = [
        $is_uefi ? () : {needle => 'netinst_bios_main_menu'},    # Back
        {needle => $needle_install1},                            # Graphical install
        {needle => $needle_install2},                            # Install
        {needle => "netinst_${variant}_dark_advanced_menu",
            submenu => \@accessible_dark_contrast_advanced_menu,},
        $is_uefi ? () : {needle => 'netinst_bios_help'},         # Help
    ];

    @accessible_dark_contrast_advanced_menu = [
        $is_uefi ? () : {needle => 'netinst_bios_dark_menu'},            # Back
        {needle => 'debianinstaller_main_menu'},                         # Graphical expert
        {needle => 'debianinstaller_select_install_lang'},               # Graphical rescue
        {needle => 'debianinstaller_download_preconfiguration_file'},    # Graphical automated
        {needle => 'text_main_menu'},                                    # Expert
        {needle => 'text_select_install_lang'},                          # Rescue
        {needle => 'text_download_preconfiguration_file'},               # Automated
    ];

    my @desktop_install_menu;
    @desktop_environment_menu = [
        {needle => 'netinst_boot_desktop', submenu => \@desktop_install_menu,},    # GNOME
        {needle => 'netinst_boot_desktop', submenu => \@desktop_install_menu,},    # KDE Plasma
        {needle => 'netinst_boot_desktop', submenu => \@desktop_install_menu,},    # LXDE
    ];

    my @desktop_install_advanced_menu;
    @desktop_install_menu = [
        {needle => 'text_select_install_lang'},                                    # Install
        {needle => 'debianinstaller_select_install_lang'},                         # Graphical install
        {needle => "netinst_${variant}_advanced_options_menu",
            submenu => \@desktop_install_advanced_menu,},
        {needle => 'speech_select_install_lang'},                                  # Speech
    ];

    @desktop_install_advanced_menu = [
        $is_uefi ? () : {needle => 'netinst_bios_dark_menu'},                      # Back
        {needle => 'debianinstaller_main_menu'},                                   # Graphical expert
        {needle => 'debianinstaller_download_preconfiguration_file'},              # Graphical automated
        {needle => 'text_main_menu'},                                              # Expert
        {needle => 'text_download_preconfiguration_file'},                         # Automated
        {needle => 'speech_main_menu'},                                            # Expert install with speech synthesis
        {needle => 'speech_download_preconfiguration_file'},                       # Automated install with speech synthesis
    ];

    return $menu;
}

sub prepare_bootmenu_debian_live_build {
    use testapi;

    my $is_uefi       = get_var('UEFI');
    my $variant       = $is_uefi ? 'uefi' : 'bios';
    my $has_installer = !check_var('LIVE_INSTALLER', 'no');

    my @advanced_install_menu;
    my @utilities_menu;
    my @menu = [
        {needle => 'live-build_live_ready'},    # Default live
        {needle => 'live-build_live_ready'},    # Failsafe live
        $has_installer ? (
            {needle => 'debianinstaller_select_install_lang'},    # Graphical installer
            {needle => 'speech_select_install_lang'},             # Speech installer
            {needle => "live-build_${variant}_advanced_install_menu",
                submenu => \@advanced_install_menu,}) : (),
        {needle => "live-build_${variant}_utilities_menu",
            submenu => \@utilities_menu,}
    ];

    my @graphical_installer_menu;
    my @text_installer_menu;
    my @speech_installer_menu;
    @advanced_install_menu = [
        $is_uefi ? () : {needle => 'live-build_bios_main_menu'},    # Back
        {needle => "live-build_${variant}_graphical-installer_menu",
            submenu => \@graphical_installer_menu,},
        {needle => "live-build_${variant}_text-installer_menu",
            submenu => \@text_installer_menu,},
        {needle => "live-build_${variant}_graphical-installer-dark_menu",
            submenu => \@graphical_installer_menu,},
        {needle => "live-build_${variant}_text-installer-dark_menu",
            submenu => \@text_installer_menu,},
        {needle => "live-build_${variant}_speech-installer_menu",
            submenu => \@speech_installer_menu,}
    ];

    @graphical_installer_menu = [
        $is_uefi ? () : {needle => 'live-build_bios_advanced_install_menu'},    # Back
        {needle => 'debianinstaller_select_install_lang'},                      # Install
        {needle => 'debianinstaller_main_menu'},                                # Expert
        {needle => 'debianinstaller_download_preconfiguration_file'},           # Automated
        {needle => 'debianinstaller_select_install_lang'},                      # Rescue;
    ];

    @text_installer_menu = [
        $is_uefi ? () : {needle => 'live-build_bios_advanced_install_menu'},    # Back
        {needle => 'text_select_install_lang'},                                 # Install
        {needle => 'text_main_menu'},                                           # Expert
        {needle => 'text_download_preconfiguration_file'},                      # Automated
        {needle => 'text_select_install_lang'},                                 # Rescue;
    ];

    @speech_installer_menu = [
        $is_uefi ? () : {needle => 'live-build_bios_advanced_install_menu'},    # Back
        {needle => 'speech_select_install_lang'},                               # Install
        {needle => 'speech_main_menu'},                                         # Expert
        {needle => 'speech_download_preconfiguration_file'},                    # Automated
        {needle => 'speech_select_install_lang'},                               # Rescue;
    ];

    if ($is_uefi) {
        @utilities_menu = [
            {needle => 'TianoCore_UEFI-firmware_main_menu'},    # UEFI Firmware settings
            {needle => 'verify_integrity'},                     # Verify integrity of the boot medium
        ];
    }
    else {
        @utilities_menu = [
            {needle => 'live-build_bios_main_menu'},            # Back
            {needle => 'syslinux_hdt-tool_main_menu'}
        ];
    }
    return @menu;
}

sub BUILD {
    my $self = shift;

    use testapi;

    # Define the boot menu for each DISTRI/FLAVOR
    if (check_var('DISTRI', 'debian')) {
        if (check_var('FLAVOR', 'mini-iso')) {
            $self->entries(prepare_bootmenu_debian_mini_iso);
        }
        elsif (check_var('FLAVOR', 'netinst-iso')) {
            $self->entries(prepare_bootmenu_debian_netinst_iso);
        }
        elsif (check_var('FLAVOR', 'live-build')) {
            $self->entries(prepare_bootmenu_debian_live_build);
        }
    }
    elsif (check_var('DISTRI', 'debian-live')) {
        $self->entries(prepare_bootmenu_debian_live_build);
    }
    elsif (check_var('DISTRI', 'debian-edu')) {
        if (check_var('FLAVOR', 'mini-iso')) {
            ...;
        }
        else {
            ...;
        }
    }
    elsif (check_var('DISTRI', 'kali')) {
        ...;
    }

    return;
}

#no Moo;
1;
